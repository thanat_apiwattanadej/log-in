<?php

require_once "lib/nusoap.php";

function createConnection($host,$username,$password,$dbname){
	// Create connection
	$con=mysqli_connect($host,$username,$password,$dbname);

	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function closeConnection($con){
	mysqli_close($con);
}

function getRules(){
	$con = createConnection("localhost","root","root","rewarddatabase");
	$q = "SELECT Rule_ID,Rule_Name,Point FROM rules";
	
	$result = mysqli_query($con,$q);
	
	closeConnection($con);
	
	$table = displayRule($result);		// Comment this line to disable displayRule
	return $table;								// Then, change the returned value to $result
}

function displayRule($result){
	$table = "<table border='1'>
	<tr>
	<th>Rule_ID</th>
	<th>Rule_Name</th>
	<th>Point</th>
	</tr>";

	while($row = mysqli_fetch_array($result))
	  {
		  $table .= "<tr>";
		  $table .= "<td>" . $row['Rule_ID'] . "</td>";
		  $table .= "<td>" . $row['Rule_Name'] . "</td>";
		  $table .= "<td>" . $row['Point'] . "</td>";
		  $table .= "</tr>";
	  }
	$table .= "</table>";
	
	return $table;
}

// create object to deal with service provider
$server = new soap_server();
$server->register("getRules");

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>